package stringnode

import (
	"fmt"
	"math/rand"
	"reflect"
	"sort"
	"testing"
)

var g *StringGraph
var want [][]string

const (
	totalSize      = 100000
	avgSize        = 20000
	duplicatesPart = 10 // 1/x
)

func init() {
	g = NewStringGraph()

	r := rand.New(rand.NewSource(3))
	var component []string
	for i := 0; i < totalSize; i++ {
		item := fmt.Sprintf("%05d", i)
		g.AddNode(item)
		if r.Intn(avgSize) == 0 {
			if len(component) > 0 {
				want = append(want, component)
			}
			component = nil
		}
		if len(component) > 0 {
			from := component[r.Intn(len(component))]
			g.AddLine(from, item)
			if r.Intn(duplicatesPart) == 0 {
				g.AddLine(from, item)
			}
		}
		component = append(component, item)

		// Connect two random nodes.
		g.AddLine(component[r.Intn(len(component))], component[r.Intn(len(component))])
	}
	want = append(want, component)
}

func TestConnectedComponents(t *testing.T) {
	got := g.ConnectedComponents()
	for _, cc := range got {
		sort.Strings(cc)
	}
	sort.Slice(got, func(i, j int) bool {
		return got[i][0] < got[j][0]
	})
	if !reflect.DeepEqual(got, want) {
		t.Errorf("got unexpected result: %v; want %v", got, want)
	}
}

func BenchmarkConnectedComponents(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = g.ConnectedComponents()
	}
}
